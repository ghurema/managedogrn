import {ADD_DOG, DELETE_DOG, EDIT_DOG, GET_DOG} from './../constants/constant';

export interface Dog {
    id: string
    name: string,
    color: string,
    breed: string,
    age: number
} 

export const addDog = (dogs: Dog) => {
  return {
      type: ADD_DOG,
      payload: {dogs},
    }
}

export const deleteDog = (id: string) => {
    return {
        type: DELETE_DOG,
        payload: {id},
      }
  }

  export const editDog = (dogs: Dog) => {
    return {
        type: EDIT_DOG,
        payload: {dogs},
      }
  }

  export const getDog = () => {
    return {
        type: GET_DOG,
        payload: {},
      }
  }
