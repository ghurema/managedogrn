interface FontThemes {
    primary: string,
    secondary: string,
    bold: string,
}

export const FONT: FontThemes = {
    primary: 'Menlo',
    secondary: 'Cochin',
    bold: 'Menlo-Bold'
};