import Home from './home'
import Dashboard from './dashboard'
import EditDogDetail from './editDogDetail'
import ViewDogDetail from './viewDogDetail'

export {Home, Dashboard, EditDogDetail, ViewDogDetail}