import React, {useState} from 'react';
import {
  KeyboardAvoidingView,
  SafeAreaView,
  StyleSheet,
  Text,
  TextInput,
  TouchableHighlight,
  View,
} from 'react-native';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {Header} from '@react-navigation/stack';
import {useDispatch} from 'react-redux';

import {addDog, Dog, editDog, deleteDog} from '../actions/dog';

export default function NewQuote(props) {
  const dispatch = useDispatch();
  const {route, navigation} = props;
  
  let dog = route.params.dogs ? route.params.dogs.dogs : null;

  const [isView, setIsView] = useState(!route.params.isView)
  const [isSaving, setIsSaving] = useState(false);
  const [dogName, setDogName] = useState(dog ? (dog.name ? dog.name : '') : '');
  const [dogColor, setDogColor] = useState(
    dog ? (dog.color ? dog.color : '') : '',
  );
  const [dogBreed, setDogBreed] = useState(dog ? (dog.breed ? dog.breed : '') : '');
  
  const [dogAge, setDogAge] = useState(dog ? (dog.age ? dog.age : '') : '');
  let edit = dog !== null;

  const onSave = () => {
    // let edit = dog !== null;
    console.log('edit:- ' + edit);
    let dog_: Dog = {
      id: '',
      name: '',
      color: '',
      breed: '',
      age: 0,
    };

    if (edit) {
      // dog_ = dog;
      // dog_['name'] = dogName;
      // dog_['color'] = dogColor;
      // dog_['breed'] = dogBreed;
      // dog_['age'] = dogAge;

      dog_ = {
        id: dog.id,
        name: dogName,
        color: dogColor,
        breed: dogBreed,
        age: dogAge,
      };
      
    } else {
      let id = generateID();
      dog_ = {
        id: id,
        name: dogName,
        color: dogColor,
        breed: dogBreed,
        age: dogAge,
      };
    }
  
          if (!edit) {
            dispatch(addDog(dog_));
            navigation.goBack();
          }
          else {
            alert('Data Save successfully')
            dispatch(editDog(dog_));
          }

          
  };

  const generateID = () => {
    let d = new Date().getTime();
    let id = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(
      /[xy]/g,
      function (c) {
        let r = (d + Math.random() * 16) % 16 | 0;
        d = Math.floor(d / 16);
        return (c == 'x' ? r : (r & 0x3) | 0x8).toString(5);
      },
    );

    return id;
  };

  const deleteItem = () => {
    dispatch(deleteDog(dog.id))
    navigation.goBack();
  }

  let disabled = dogName.length > 0 && dogColor.length > 0 && dogAge.length > 0 ? false : true;

  const saveEditButton = (action) => {
    return (<TouchableHighlight
      style={[styles.button]}
      disabled={disabled}
      onPress={onSave}
      underlayColor="rgba(0, 0, 0, 0)">
      <Text
        style={[
          styles.buttonText,
          {color: disabled ? 'rgba(255,255,255,.5)' : '#FFF'},
        ]}>
        {action}
      </Text>
    </TouchableHighlight>)
  }
  return (
    <KeyboardAvoidingView
      keyboardVerticalOffset={Header.HEIGHT}
      style={styles.flex}
      behavior="padding">
      <SafeAreaView style={styles.flex}>
        <View style={styles.flex}>
          <TextInput
            onChangeText={text => setDogName(text)}
            placeholder={'Dog Name'}
            
            editable={isView}
            style={[styles.author]}
            value={dogName}
          />
          <TextInput
            onChangeText={text => setDogColor(text)}
            placeholder={'Dog color'}
            
            editable={isView}
            style={[styles.author]}
            value={dogColor}
          />
          <TextInput
            onChangeText={text => setDogBreed(text)}
            placeholder={'Dog breed'}
            
            editable={isView}
            style={[styles.author]}
            value={dogBreed}
          />
          <TextInput
            onChangeText={text => setDogAge(text)}
            placeholder={'Dog age'}
            editable={isView}
            style={[styles.author]}
            value={dogAge}
          />
        </View>

{!isView ? (<TouchableHighlight
              style={[styles.button]}
              disabled={disabled}
              onPress={() => setIsView(true)}
              underlayColor="rgba(0, 0, 0, 0)">
              <Text
                style={[
                  styles.buttonText,
                  {color: disabled ? 'rgba(255,255,255,.5)' : '#FFF'},
                ]}>
                Edit Data
              </Text>
            </TouchableHighlight>) :
        (<View style={styles.buttonContainer}>
          <View style={{flex: 1, alignItems: 'flex-end', flexDirection:'row'}}>          
          {edit ? saveEditButton('Edit'):saveEditButton('Save')}
            <TouchableHighlight
              style={[styles.button]}
              disabled={disabled}
              onPress={deleteItem}
              underlayColor="rgba(0, 0, 0, 0)">
              <Text
                style={[
                  styles.buttonText,
                  {color: disabled ? 'rgba(255,255,255,.5)' : '#FFF'},
                ]}>
                Delete
              </Text>
            </TouchableHighlight>
          </View>
        </View>)}
      </SafeAreaView>
    </KeyboardAvoidingView>
  );
}

const styles = StyleSheet.create({
  flex: {
    flex: 1,
  },

  buttonContainer: {
    height: 70,
    flexDirection: 'row',
    padding: 12,
    backgroundColor: 'white',
  },

  count: {
    fontFamily: 'HelveticaNeue-Medium',
    fontSize: 17,
    color: '#6B9EFA',
  },

  button: {
    width: 80,
    height: 44,
    margin: 5,
    borderRadius: 8,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#6B9EFA',
  },

  buttonText: {
    fontFamily: 'HelveticaNeue-Medium',
    fontSize: 16,
  },

  author: {
    fontSize: 20,
    lineHeight: 22,
    fontFamily: 'Helvetica Neue',
    height: 80,
    padding: 16,
    backgroundColor: 'white',
  },

  text: {
    fontSize: 30,
    lineHeight: 33,
    fontFamily: 'Helvetica Neue',
    color: '#333333',
    padding: 16,
    paddingTop: 16,
    minHeight: 170,
    borderTopWidth: 1,
    borderColor: 'rgba(212,211,211, 0.3)',
  },
});
