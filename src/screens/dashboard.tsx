import React, {FC, useState, useRef, useEffect} from 'react';
import {View, TouchableHighlight, FlatList} from 'react-native';
import {FONT} from '../constants/themes';
import {Add} from '../../assets/index'
import {RenderDog} from '../components/index';
// import {FlatList} from 'react-native-gesture-handler';
import { FloatingAction } from "react-native-floating-action";
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useDispatch, useSelector } from 'react-redux';

import { getDog, deleteDog } from "../actions/dog";

const Dashboard: FC = (props:any) => {
  const dispatch = useDispatch();
  // const [dogs, setDogs] = useState([{icon:'', name:'Bruno', age:'12'}]);
const { navigation } = props
  const dataReducer = useSelector((state: any) => state.dataReducer);
  const { dogs } = dataReducer;


  const actions = [
    {
      text: "Accessibility",
      icon: Add,
      name: "bt_accessibility",
      position: 1
    }
  ];
  
 
  useEffect(() => {getDog()}, []);

const onClickEdit = item => {
  navigation.navigate('editDogDetail',{title:"New Dog", dogs:item, isView: true })
}
  return (
    <View style={{ flex: 1 }}>
      <FlatList
      contentContainerStyle={{ flexGrow: 1 }}
        data={dogs}
        renderItem={({item}) => {
          let dog = item.dogs
          return (
            <TouchableHighlight
              
              onPress={() => onClickEdit(item)}
             >
          <RenderDog
            icon={''}
            name={dog.name}
            age={dog.age}
          />
          </TouchableHighlight>
        )}}
      />
       <FloatingAction
            actions={actions}
            overrideWithAction
            onPressItem={name => {
              navigation.navigate('editDogDetail',{title:"New Dog"});
            }}
          />
    </View>
  );
};

export default Dashboard;
