import React, {FC, useState, useRef, useEffect} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {FONT} from '../constants/themes';
import {Button} from './../components/index';

const Home: FC = props => {
  const [time, setTime] = useState(new Date().toLocaleTimeString());
  const [date, setDate] = useState(new Date().toLocaleDateString());
  const secondsPassed = useRef(0);

  

  useEffect(() => {
    const timeout = setTimeout(() => {
      const date = new Date();
      secondsPassed.current = secondsPassed.current + 1;
      setTime(date.toLocaleTimeString());
      setDate(date.toLocaleDateString());
    }, 1000);
    return () => {
      clearTimeout(timeout);
    };
  }, [time]);

  return (
    <View style={styles.container}>
      <View style={{alignItems: 'center'}}>
        <Text style={styles.time}>{time}</Text>
        <Text style={styles.time}>Hello</Text>
        <Text style={styles.time}>{date}</Text>
      </View>
      <Text style={styles.title}>Manage your Dog (s)</Text>
      <Button
        title={"Let's begin"}
        onPress={() => {
          props.navigation.navigate('dashboard');
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-around',
    alignItems: 'center',
  },
  time: {
    fontSize: 20,
    fontFamily: FONT.secondary,
  },
  title: {
    fontFamily: FONT.bold,
    fontSize: 22,
  },
});
export default Home;
