import React, {FC} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {Home, Dashboard, EditDogDetail, ViewDogDetail} from '../screens';

const Stack = createNativeStackNavigator();

const AppStack : FC = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen name="home" component={Home} options={{headerShown: false}} />
         <Stack.Screen name="dashboard" component={Dashboard} options={{ title: 'My dog (s)' }}/>
        <Stack.Screen name="editDogDetail" component={EditDogDetail} options={{ title: 'My dog (s)' }}/>
        {/* <Stack.Screen name="ViewDogDetail" component={ViewDogDetail} /> */}
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default AppStack;
