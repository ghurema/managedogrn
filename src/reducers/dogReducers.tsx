import {combineReducers} from 'redux';

import {ADD_DOG, DELETE_DOG, EDIT_DOG, GET_DOG} from './../constants/constant';
import {Dog} from '../actions/dog';
let dataState = {dogs: []};

const dataReducer = (state = dataState, action: any) => {
 
  switch (action.type) {
    case ADD_DOG:
     { 
     
      let dog = action.payload;
      let clone = JSON.parse(JSON.stringify(state.dogs));
      clone.unshift(dog);
      return {...state, dogs: clone};
     }
    case DELETE_DOG:
      {
      let {id} = action.payload;
      
      let clone = JSON.parse(JSON.stringify(state.dogs));
      const index = clone.findIndex((obj:any) => obj.dogs.id === id);
      
      if (index !== -1) clone.splice(index, 1);
      return {...state, dogs: clone};
      }
    case EDIT_DOG:
      {
      let dog = action.payload;
      let clone = JSON.parse(JSON.stringify(state.dogs));
      const index = clone.findIndex((obj: any) => obj.dogs.id === dog.dogs.id);
      if (index !== -1) clone[index] = dog;
      return {...state, dogs: clone};
      }
    case GET_DOG:
      {
      // let {dogs} = action.payload;
      return {...state};
      }
    default:
      return state;
  }
};

const rootReducer = combineReducers({dataReducer});

export default rootReducer;
