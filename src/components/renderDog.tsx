import React, { FC } from 'react'
import {View, Text, StyleSheet, Dimensions, Image} from 'react-native'
import { Button } from '.';
import {Bruno} from '../../assets/index'
const  {width, height} = Dimensions.get('screen')

interface Props {
    icon: string;
    name: string;
    age: string;
}

const App : FC <Props> = (props) => {
    return (
        <View style={styles.container}>
            <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                <Text style={{width: '60%'}}>{props.name}</Text>
                <Text>{props.age}</Text>
            </View>
        </View>
    )
}

export default App;

const styles = StyleSheet.create({
    container: {
        width: width / 1.1,
        alignSelf: 'center',
        marginVertical: 10,
        padding: 20,
        borderRadius: 10,
        backgroundColor: '#fff',
        shadowOffset: {
            width: 3,
            height: 3
        },
        shadowColor: '#ccc',
        shadowOpacity: 0.9
    }
})