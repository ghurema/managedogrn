/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {FC} from 'react';
import AppStack from './src/navigation/appStack';
import { Provider } from 'react-redux';
import store from './src/store/configureStore'

const App: FC = () => {
  
  return (
    <Provider store={store}>
    <AppStack/>
    </Provider>
  );
};



export default App;
